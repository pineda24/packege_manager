var assert = require('assert');
var sumar = require('../index');

//ASERT = Afirmacion
// 50 % test

describe('Probar la suma de dos numeros', () => {
    // Afirmacion que 5 + 5 = 10
    it('Cinco mas cinco es 10', () => {
        assert.equal(10,sumar(5,5));
    });
    // Afirmamos que cinco mas siete no es igual a 10
    it('Cinco mas siete no es igual a 10', () => {
        assert.notEqual(10,sumar(5,7));
    });
});